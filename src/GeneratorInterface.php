<?php

namespace Eactive\Puppeteer;

interface GeneratorInterface
{
    /**
     * for valid options see puppeteer documentation.
     *
     * @param string $html
     * @param array  $options
     *
     * @return string
     */
    public function generate(string $html, array $options = []): string;
}
