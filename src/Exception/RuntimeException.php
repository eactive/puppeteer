<?php

namespace Eactive\Puppeteer\Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}
