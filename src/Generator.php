<?php

namespace Eactive\Puppeteer;

use Symfony\Component\Process\Process;
use Eactive\Puppeteer\Exception\RuntimeException;

class Generator implements GeneratorInterface
{
    const TEMPLATE = <<<'JAVASCRIPT'
const puppeteer = require('puppeteer');

(async () => {
    try { 
        const browser = await puppeteer.launch(%s);

        try {
            const page = await browser.newPage();
            await page.setContent(%s, %s);
    
            process.stdout.write(await page.pdf(%s));
        } finally {
            await browser.close();
        }
    } catch (error) {
        process.exitCode = 1;
    
        console.error(error);
    }
})()
JAVASCRIPT;

    /**
     * @var string
     */
    private $node;

    /**
     * @var array
     */
    private $options;

    /**
     * @param string $node
     * @param array  $options the default options @see generate for more info
     */
    public function __construct(string $node, array $options = [])
    {
        $this->node = $node;

        $this->options = $options;
        $this->options['launch'] = $this->options['launch'] ?? [];
    }

    /**
     * The options are used for the following puppeteer API calls. No processing or validation is
     * done on the options the are used as supplied and will only be json encode.
     *
     * The launch options must be in a separate array with the key launch.
     *
     * - puppeteer.launch
     * - page.setContent
     * - page.pdf
     *
     * See the puppeteer documentation for valid options.
     *
     * https://github.com/puppeteer/puppeteer
     *
     * @param string $html
     * @param array  $options
     *
     * @return string
     */
    public function generate(string $html, array $options = []): string
    {
        $process = new Process($this->node);

        $process->setInput($this->generateScript($html, $options));
        $process->run();

        if ($process->isSuccessful()) {
            return $process->getOutput();
        }

        throw new RuntimeException($process->getErrorOutput());
    }

    private function generateScript(string $html, array $options): string
    {
        $launch = ($options['launch'] ?? []) + $this->options['launch'];
        $options = $options + $this->options;

        $wait = [];

        $wait['timeout'] = $options['timeout'] ?? null;
        $wait['waitUntil'] = $options['waitUntil'] ?? null;

        $wait = array_filter($wait);

        unset($options['launch']);
        unset($options['timeout']);
        unset($options['waitUntil']);

        return sprintf(
            self::TEMPLATE,
            json_encode($launch),
            json_encode($html),
            json_encode($wait),
            json_encode($options)
        );
    }
}
